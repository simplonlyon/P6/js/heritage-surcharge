'use strict';
import { Vehicle } from './vehicle';

/**
 * @param {string} color Le code couleur au format hexadecimal.
 * @param {string} brand La marque de la voiture.
 * @param {number} power La puissance de la voiture en kw.
 */
export class Car extends Vehicle {
  constructor(color, brand, power){
    super(color, brand, 4, 5); // on appel le constructeur de la classe parent grâce à super()
    this.power = power;
    this.sunroof = false;
    this.started = false;
  }

  start(){
    this.started = true;
    console.log('i\'m started');
  }
  toggleSunroof(){
    this.sunroof = !this.sunroof;
  }
  move(){ // on surcharge la méthode héritée move()
    if (this.started) {
      super.move(); // on peut appeler les méthodes du parent grâce à super.method()
    } else {
      console.log('you must start me before !');
    }
  }
}