'use strict';

/**
 * @param {string} color Le code couleur au format hexadecimal.
 * @param {string} brand La marque du véhicule.
 * @param {number} wheels Le nombre de roues du véhicule.
 * @param {number} seats Le nombre de siège du véhicule.
 */
export class Vehicle {
  constructor(color, brand, wheels, seats){
    this.color = color;
    this.brand = brand;
    this.wheels = wheels;
    this.seats = seats;
  }

  move(){
    console.log('i\'m moving !')
  }
}