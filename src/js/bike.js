'use strict';
import { Vehicle } from "./vehicle";

/**
 * @param {string} color Le code couleur au format hexadecimal.
 * @param {string} brand La marque du vélo.
 * @param {number} basket Si le vélo à un panier.
 * @param {number} saddleHeight La hauteur de la selle en cm.
 */
export class Bike extends Vehicle {
  constructor(color, brand, basket = false, saddleHeight = 0){
    super(color, brand, 2, 1); //on appel le constructeur de la classe parent grâce à super()
    this.basket = basket;
    this.saddleHeight = saddleHeight;
  }

  adjustSaddle(height){
    this.saddleHeight = height;
    console.log(`saddle is now adjusted to : ${this.saddleHeight}cm`);
  }
}