'use strict';
import { Car } from './car';
import { Bike } from './bike';

console.log('Testing bike :');
let testBike = new Bike('#ff0000', 'Lapierre');

testBike.adjustSaddle(10);
testBike.move();
console.log(testBike);

console.log('\nTesting car :');
let testCar = new Car('#bada55', 'Peugeot', 15000);

testCar.move();
testCar.start();
testCar.move();
console.log(testCar);
