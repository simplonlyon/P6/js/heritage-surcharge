module.exports = {
  mode: 'development',
  entry: {
    main: './src/js/main.js'
  },
  devtool: 'inline-source-map'
};
