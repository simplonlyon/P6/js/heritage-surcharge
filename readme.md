# Héritage et surcharge
Projet JS pour illustrer le principe de surcharge des méthodes héritées.

## Démarrage du projet
1. cloner le projet.
1. ouvrir le dossier du projet dans un terminal.
1. lancer la commande `npm install`.
1. lancer la transpilation de webpack avec la commande `npm run dev`.
1. ouvrir le fichier `src/index.html` dans le navigateur.

## Exercice
On souhaite implémenter des classes `Bike` et `Car` toutes deux filles d'une classe `Vehicle`

### 1) La classe `Vehicle`
Implémenter la classe suivante :

![Vehicle class](https://yuml.me/diagram/plain/class/[Vehicle|color:string;brand:string;wheels:number;seats:number|move()].svg)
- la méthode `move()` écrit dans la console "i'm moving"

### 2) La classe `Bike`
Implémenter la classe suivante :

![Bike class](https://yuml.me/diagram/plain/class/[Bike|basket:boolean;saddleHeight:number|adjustSaddle()].svg)
- la méthode `adjustSaddle()` prend un nombre en paramètre, elle change la valeur de la propriété `saddleHeight` puis écrit dans la console "saddle is now adjusted to : `saddleHeight`cm"

Faire en sorte que la classe `Bike` hérite de la classe `Vehicle` :

![Bike and Vehicle classes](https://yuml.me/diagram/plain/class/[Vehicle|color:string;brand:string;wheels:number;seats:number|move()]^-[Bike|basket:boolean;saddleHeight:number|adjustSaddle()].svg)

### 3) La classe `Car`
Implémenter la classe suivante :

![Car class](https://yuml.me/diagram/plain/class/[Car|power:number;sunroof:boolean=false;started:boolean=false|start();toggleSunroof()].svg)
- la méthode `start()` met la propriété `started` à `true` puis écrit dans la console "i'm started"
- la méthode `toggleSunroof()` met la propriété `sunroof` à `true`

Faire en sorte que la classe `Car` hérite de la classe `Vehicle` :

![Bike, Car and Vehicle classes](https://yuml.me/diagram/plain/class/[Vehicle|color:string;brand:string;wheels:number;seats:number|move()]^-[Bike|basket:boolean;saddleHeight:number|adjustSaddle()],[Vehicle]^-[Car|power:number;sunroof:boolean=false;started:boolean=false|start();toggleSunroof()].svg)

Si on appel la méthode `move()` sur une instance de la classe `Car` la console va afficher "i'm moving", or une voiture ne peut pas se déplacer sans être démarrée...

Pour éviter ce problème, il faut redéfinir la méthode `move()` au sein de la classe `Car` :

![Car class](https://yuml.me/diagram/plain/class/[Car|power:number;sunroof:boolean=false;started:boolean=false|start();toggleSunroof();move()].svg)

Cette méthode doit s'assurer que la propriété `started` est à `true` avant d'appeler la méthode `move()` du parent, sans quoi la console affiche "you must start me before !".

Le fait de rédéfinir une méthode du parent dans la classe fille s'appel une **surcharge**.

Voici le diagrame final :

![Bike, Car and Vehicle classes](https://yuml.me/diagram/plain/class/[Vehicle|color:string;brand:string;wheels:number;seats:number|move()]^-[Bike|basket:boolean;saddleHeight:number|adjustSaddle()],[Vehicle]^-[Car|power:number;sunroof:boolean=false;started:boolean=false|start();toggleSunroof();move()].svg)